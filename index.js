
        var toastList = []
        var newMarker = []
        var layerActive = false;
        var layerActiveData = false;
        var layerType = 1
        var geosjonsetting = {
                onEachFeature: onEachFeature,
                style: function (feature) {
                    return {
                        fillColor: '#102D4F',
                        weight: 2,
                        opacity: 0.5,
                        color: '#1B3B62',
                        fillOpacity: 0.7
                    };
                }
            }
        var marker1 = L.icon({
            iconUrl: 'asset/marker1.svg',
            iconSize: [38, 95],
        });
        var marker2 = L.icon({
            iconUrl: 'asset/marker2.svg',
            iconSize: [38, 95],
        });
        var marker3 = L.icon({
            iconUrl: 'asset/marker3.svg',
            iconSize: [38, 95],
        });




        var map = L.map('map-wrapper',{
            center: [-3.0238526699136,117.09878620274043,5.5],  
            zoom: 5.5,
            zoomSnap: 0.25,
            dragging: true,
            doubleClickZoom: true,
            boxZoom: true,
            zoomControl: false
        });
        map.setMinZoom(4.74)
        map.setMaxZoom(13)

        var geojsonLayer;
        

        function geojsongelombang(){
            geojsonLayer = [];
            for(let i=1; i<=21 ; i++){
                geojsonLayer.push(new L.GeoJSON.AJAX(`geo_gelombang/gel${i}.json`,geosjonsetting))
                geojsonLayer[geojsonLayer.length-1].addTo(map);
            }
        }

        function geojson1(){
            geojsonLayer = new L.GeoJSON.AJAX("geojson-provinsi.json",geosjonsetting);       
            geojsonLayer.addTo(map);
        }
        function geojson2(){
            geojsonLayer = new L.GeoJSON.AJAX("geojson-kota.json",geosjonsetting);       
            geojsonLayer.addTo(map);
        }


        function resetCenter(){
            map.setView(new L.LatLng(-3.0238526699136, 117.09878620274043), 5.5);
            closeCuaca()
        }


        function whenHover(e){
            var layer = e.target;

			layer.setStyle({
                fillColor: '#D1AB60',
				weight: 3,
				color: '#EAC881',
				dashArray: '',
                opacity: 1,
				fillOpacity: 0.7
			});

			if (!L.Browser.ie && !L.Browser.opera) {
				layer.bringToFront();
			}

            // console.log(layer.feature.properties)
            // console.log(e)
        }

        function leaveHover(e){
            var layer = e.target;

            if(layerType == 4 || layerType == 6){
                if(layerActive != layer.feature.properties.ID_1){
                    geojsonLayer.resetStyle(layer);
                }
            }else if(layerType == 5){
                $.each(geojsonLayer, (i, d)=>{
                    d.resetStyle(layer);
                })
            }
        }
        
        function whenClicked(e) {
            var layer = e.target;
            
            if(layerType == 4){
                if(layerActive != layer.feature.properties.ID_1){

                    if(layerActiveData){
                        geojsonLayer.resetStyle(layerActiveData);
                    }
                    
                    $('#cuaca-location').text(layer.feature.properties.NAME_1)
                    $('#cuaca-modal').show()

                    map.panTo(new L.LatLng(e.latlng.lat, parseFloat(e.latlng.lng)+0.21751819929076532));

                    layerActive = layer.feature.properties.ID_1
                    layerActiveData = layer

                    layer.setStyle({
                        fillColor: '#D1AB60',
                        weight: 3,
                        color: '#EAC881',
                        dashArray: '',
                        opacity: 1,
                        fillOpacity: 0.7
                    });

                    if (!L.Browser.ie && !L.Browser.opera) {
                        layer.bringToFront();
                    }
                }
            }else if(layerType == 5){
                if(layerActive != layer.feature.properties.WP_1){

                    if(layerActiveData){
                        $.each(geojsonLayer, (i, d)=>{
                            d.resetStyle(layer);
                        })
                    }
                    
                    $('#gelombang-location').text(layer.feature.properties.WP_IMM)
                    $('#gelombang-modal').show()

                    map.panTo(new L.LatLng(e.latlng.lat, parseFloat(e.latlng.lng)+0.21751819929076532));

                    layerActive = layer.feature.properties.WP_1
                    layerActiveData = layer

                    layer.setStyle({
                        fillColor: '#D1AB60',
                        weight: 3,
                        color: '#EAC881',
                        dashArray: '',
                        opacity: 1,
                        fillOpacity: 0.7
                    });

                    if (!L.Browser.ie && !L.Browser.opera) {
                        layer.bringToFront();
                    }
                }
            }else if(layerType == 6){
                if(layerActive != layer.feature.properties.ID_1){

                    if(layerActiveData){
                        geojsonLayer.resetStyle(layerActiveData);
                    }
                    
                    $('#covid-location').text(layer.feature.properties.NAME_1)
                    $('#covid-modal').show()

                    map.panTo(new L.LatLng(e.latlng.lat, parseFloat(e.latlng.lng)+0.21751819929076532));

                    layerActive = layer.feature.properties.ID_1
                    layerActiveData = layer

                    layer.setStyle({
                        fillColor: '#D1AB60',
                        weight: 3,
                        color: '#EAC881',
                        dashArray: '',
                        opacity: 1,
                        fillOpacity: 0.7
                    });

                    if (!L.Browser.ie && !L.Browser.opera) {
                        layer.bringToFront();
                    }
                }
            }

            console.log(layer)
        }

        function onEachFeature(feature, layer) {
            layer.on({
                click: whenClicked,
                mouseover: whenHover,
                mouseout: leaveHover
            });
        }

        function closeCuaca(){
            try{
                geojsonLayer.resetStyle(layerActiveData);
            }catch(e){

            }
            layerActive = false
            layerActiveData = false
            L.Marker.stopAllBouncingMarkers();
            $('.cuaca-modal').hide()
        }

        // map.on('moveend', function() {
        
        // });

        function desaWisata(t = false){
            if(t){
                map.removeLayer(newMarker['desaWisata']);
            }else{
                var r = L.markerClusterGroup();
                $.getJSON( "asset/desa_wisata.json", function( data ) {
                    $.each( data.Markers, function( key, val ) {
                        var r2 = new L.marker([val.Y, val.X], {icon: marker1})
                        r.addLayer(r2.setBouncingOptions({
                            bounceHeight : 10,
                            bounceSpeed  : 54,
                            exclusive    : true, 
                        }).on('click', selectMarker));
                        map.addLayer(r);
                        
                        newMarker['desaWisata'] = r
                    });
                });
            }  
        }

        function atraksiBuatan(t = false){
            if(t){
                map.removeLayer(newMarker['atraksiBuatan']);
            }else{
                var r = L.markerClusterGroup();
                $.getJSON( "asset/atraksi.json", function( data ) {
                    $.each( data.Markers, function( key, val ) {
                        var r2 = new L.marker([val.Y, val.X], {icon: marker1})
                        r.addLayer(r2.setBouncingOptions({
                            bounceHeight : 10,
                            bounceSpeed  : 54,
                            exclusive    : true, 
                        }).on('click', selectMarker));
                        map.addLayer(r);
                        
                        newMarker['atraksiBuatan'] = r
                    });
                });
            }   
        }

        function atraksiAlam(t = false){
            if(t){
                map.removeLayer(newMarker['atraksiAlam']);
            }else{
                var r = L.markerClusterGroup();
                $.getJSON( "asset/atraksiAlam.json", function( data ) {
                    $.each( data.Markers, function( key, val ) {
                        var r2 = new L.marker([val.Y, val.X], {icon: marker1})
                        r.addLayer(r2.setBouncingOptions({
                            bounceHeight : 10,
                            bounceSpeed  : 54,
                            exclusive    : true, 
                        }).on('click', selectMarker));
                        map.addLayer(r);
                        
                        newMarker['atraksiAlam'] = r
                    });
                });
            }   
        }

        function kerajinanTangan(t = false){
            if(t){
                map.removeLayer(newMarker['kerajinanTangan']);
            }else{
                var r = L.markerClusterGroup();
                $.getJSON( "asset/kerajinan_tangan.json", function( data ) {
                    $.each( data.Markers, function( key, val ) {
                        var r2 = new L.marker([val.Y, val.X], {icon: marker2})
                        r.addLayer(r2.setBouncingOptions({
                            bounceHeight : 10,
                            bounceSpeed  : 54,
                            exclusive    : true, 
                        }).on('click', selectMarker));
                        map.addLayer(r);
                        
                        newMarker['kerajinanTangan'] = r
                    });
                });
            }  
        }
        function fotografi(t = false){
            if(t){
                map.removeLayer(newMarker['fotografi']);
            }else{
                var r = L.markerClusterGroup();
                $.getJSON( "asset/fotografi.json", function( data ) {
                    $.each( data.Markers, function( key, val ) {
                        var r2 = new L.marker([val.Y, val.X], {icon: marker2})
                        r.addLayer(r2.setBouncingOptions({
                            bounceHeight : 10,
                            bounceSpeed  : 54,
                            exclusive    : true, 
                        }).on('click', selectMarker));
                        map.addLayer(r);
                        
                        newMarker['fotografi'] = r
                    });
                });
            }  
        }
        function penerbitan(t = false){
            if(t){
                map.removeLayer(newMarker['penerbitan']);
            }else{
                var r = L.markerClusterGroup();
                $.getJSON( "asset/penerbitan.json", function( data ) {
                    $.each( data.Markers, function( key, val ) {
                        var r2 = new L.marker([val.Y, val.X], {icon: marker2})
                        r.addLayer(r2.setBouncingOptions({
                            bounceHeight : 10,
                            bounceSpeed  : 54,
                            exclusive    : true, 
                        }).on('click', selectMarker));
                        map.addLayer(r);
                        
                        newMarker['penerbitan'] = r
                    });
                });
            }  
        }

        function bencana(t = false){
            if(t){
                map.removeLayer(newMarker['bencana']);
            }else{
                var r = L.markerClusterGroup();
                $.getJSON( "asset/bencana.json", function( data ) {
                    $.each( data.Markers, function( key, val ) {
                        var r2 = new L.marker([val.Y, val.X], {icon: marker3})
                        r.addLayer(r2.on('click', selectMarker));
                        map.addLayer(r);
                        
                        newMarker['bencana'] = r
                    });
                });
            }   
        }
        function banjir(t = false){
            if(t){
                map.removeLayer(newMarker['banjir']);
            }else{
                var r = L.markerClusterGroup();
                $.getJSON( "asset/banjir.json", function( data ) {
                    $.each( data.Markers, function( key, val ) {
                        var r2 = new L.marker([val.Y, val.X], {icon: marker1})
                        r.addLayer(r2.on('click', selectMarker));
                        map.addLayer(r);
                        
                        newMarker['banjir'] = r
                    });
                });
            }   
        }
        function kebakaran(t = false){
            if(t){
                map.removeLayer(newMarker['kebakaran']);
            }else{
                var r = L.markerClusterGroup();
                $.getJSON( "asset/kebakaran.json", function( data ) {
                    $.each( data.Markers, function( key, val ) {
                        var r2 = new L.marker([val.Y, val.X], {icon: marker1})
                        r.addLayer(r2.on('click', selectMarker));
                        map.addLayer(r);
                        
                        newMarker['kebakaran'] = r
                    });
                });
            }   
        }

        function selectMarker(e){
            console.log(e)
            if(layerType == 1){
                $('#wisata-modal').show()
                this.toggleBouncing();
            }else if(layerType == 2){
                $('#ekonomi-modal').show()
                this.toggleBouncing();
            }else if(layerType == 3){
                $('#bencana-modal').show()
            }
            map.panTo(new L.LatLng(e.latlng.lat, parseFloat(e.latlng.lng)+0.21751819929076532));
        }


        function zoomIn(){
            map.zoomIn();
        }
        function zoomOut(){
            map.zoomOut();
        }


        function switchMap(t){
            map.eachLayer(function (layer) {
                map.removeLayer(layer);
            });
            L.Marker.stopAllBouncingMarkers();
            resetCenter()

            layerType = t

            $('.legend-itm').removeClass('active')
            $(event.currentTarget).addClass('active')

            switch(t){
                case 1:
                    $('#tab-legend-btn-2').show()
                    $('#tab-legend-btn-1').removeClass('single')
                    loadMap3()
                    desaWisata()
                break;
                case 2:
                    $('#tab-legend-btn-2').show()
                    $('#tab-legend-btn-1').removeClass('single')
                    loadMap3()
                    kerajinanTangan()
                break;
                case 3:
                    $('#tab-legend-btn-2').show()
                    $('#tab-legend-btn-1').removeClass('single')
                    loadMap3()
                    bencana()
                    bencanaNotif(true)
                break;
                case 4:
                    $('#tab-legend-btn-2').hide()
                    $('#tab-legend-btn-1').addClass('single')
                    geosjonsetting = {
                        onEachFeature: onEachFeature,
                        style: function (feature) {
                            return {
                                fillColor: '#102D4F',
                                weight: 2,
                                opacity: 0.5,
                                color: '#1B3B62',
                                fillOpacity: 0.7
                            };
                        }
                    }
                    loadMap1()
                    geojson1()
                break;
                case 5:
                    $('#tab-legend-btn-2').hide()
                    $('#tab-legend-btn-1').addClass('single')
                    geosjonsetting = {
                        onEachFeature: onEachFeature,
                        style: function (feature) {
                            return {
                                fillColor: '#102D4F',
                                weight: 2,
                                opacity: 0.5,
                                color: '#1B3B62',
                                fillOpacity: 0.7
                            };
                        }
                    }
                    loadMap1()
                    geojsongelombang()
                break;
                case 6:
                    $('#tab-legend-btn-2').hide()
                    $('#tab-legend-btn-1').addClass('single')
                    geosjonsetting = {
                        onEachFeature: onEachFeature,
                        style: function (feature) {
                            return {
                                fillColor: '#A92525',
                                weight: 2,
                                opacity: 0.5,
                                color: '#1B3B62',
                                fillOpacity: 0.7
                            };
                        }
                    }
                    loadMap4()
                    geojson1()
                break;
                case 7:
                    $('#tab-legend-btn-2').hide()
                    $('#tab-legend-btn-1').addClass('single')
                    loadMap5()
                break;
            }
        }

        function loadMap1(){
            L.tileLayer(
            'https://api.mapbox.com/styles/v1/adiantodu/ckxyzklgrzvwq15luyhrbv72t/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoiYWRpYW50b2R1IiwiYSI6ImNreHl3dXEyYjR1MjYyd3BubnY4dTdtcWQifQ.Ls46BIK69rJM8gDBIfzv-Q', {
                tileSize: 512,
                zoomOffset: -1,
                attribution: '© <a href="https://www.mapbox.com/contribute/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
            }).addTo(map);
        }

        function loadMap2(){
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);
        }
        
        function loadMap3(){
            L.tileLayer(
            'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
                attribution: '&copy; <a href="http://www.esri.com/">Esri</a>'
            }).addTo(map);
            L.tileLayer(
            'https://services.arcgisonline.com/arcgis/rest/services/Reference/World_Boundaries_and_Places/MapServer/tile/{z}/{y}/{x}', {
                attribution: '&copy; <a href="http://www.esri.com/">Esri</a>'
            }).addTo(map);
        }
        function loadMap4(){
            L.tileLayer(
            'https://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {
                attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ'
            }).addTo(map);
        }
        function loadMap5(){
            L.tileLayer(
            'https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
                attribution: 'Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
            }).addTo(map);
        }
        

        function toggleMenu(){
            $('.nav-menu').toggleClass('active')
        }

        function toastTemplate(d){
            return `
            <div class="toast-content" id="toast-${d.id}">
                <div class="t-16 t-600">
                    GEMPA BUMI
                </div>
                <div class="t-12 line-clamp clamp-4">
                    Gempa Mag:3.8, 03-Jan-22 22:57:42 WIB, Lok:0.71 LS, 131.48 BT (Pusat gempa berada di laut 28 km timur laut Kota Sorong), Kedlmn:2 Km Dirasakan (MMI) III Sorong
                </div>
            </div>`
        }

        function bencanaNotif(t = false){
            if(t){
                $('.map-notif').hide()
            }else{
                $('.map-notif').show()
            }
        }

        function testToast(){
            var d = {
                'id': (Math.random()*10).toFixed()
            }
            toastList.push(d.id)
            if(layerType != 3){
                bencanaNotif()
            }
            $('.toast').append(toastTemplate(d))
            setTimeout(()=>{
                $(`#toast-${d.id}`).remove()
            },2500)
        }


        function updateTime(){
            var dayList = ['Minggu','Senin','Selasa','Rabu','Kamis',"Jum'at",'Sabtu']
            var monthList = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember']
            var currentTime = new Date()
            var hours = currentTime.getHours()
            var minutes = currentTime.getMinutes()
            var day = currentTime.getDay()
            var date = currentTime.getDate()
            var month = currentTime.getMonth()
            var year = currentTime.getFullYear()
            if (minutes < 10){
                minutes = "0" + minutes
            }
            var t_str = hours + " : " + minutes + " ";
            if(hours > 11){
                t_str += "PM";
            } else {
                t_str += "AM";
            }
            $('.nav-date > span:first-child').text(t_str)
            $('.nav-date > span:last-child').text(`${dayList[day]}, ${date} ${monthList[month]} ${year}`)
        }


        function toggleFilter(){
            $('.filter-menu').toggle()
        }


        function legendTab(p){
            
            $('.legend-content').removeClass('active')
            $('.tab-legend').removeClass('active')
            $('#tab-legend-btn-'+p).toggleClass('active')
            
            console.log(layerType)
            if(p == 1 && layerType < 4){
                $('#tab-legend-'+p).addClass('active')
            }else{
                $('#tab-legend-'+(layerType+1)).addClass('active')
            }

            if(p == 1){
                $('.legend-tool').show()
            }else{
                $('.legend-tool').hide()
            }
        }


        function changeFilter(p){
            var act = $(event.currentTarget).attr('class').search('active') > -1 ? true : false
            $(event.currentTarget).toggleClass('active')

            switch(p){
                case 1:
                    if(layerType == 1){
                        desaWisata(act)
                    }else if(layerType == 2){
                        kerajinanTangan(act)
                    }else{
                        bencana(act)
                    }
                break;
                case 2:
                    if(layerType == 1){
                        atraksiBuatan(act)
                    }else if(layerType == 2){
                        fotografi(act)
                    }else{
                        banjir(act)
                    }
                break;
                case 3:
                    if(layerType == 1){
                        atraksiAlam(act)
                    }else if(layerType == 2){
                        penerbitan(act)
                    }else{
                        kebakaran(act)
                    }
                break;
            }
        }